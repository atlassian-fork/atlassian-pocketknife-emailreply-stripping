package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Unit tests for each regex. <br/>
 * 2. Normal reply. <br/>
 * 3. With cc reply. <br/>
 * 4. With long fullname reply. <br/>
 * 5. With malicious content reply. <br/>
 * 6. Normal forward. <br/>
 * 7. With cc forward. <br/>
 * 8. With long fullname forward. <br/>
 * 9. With malicious forward. <br/>
 * </p>
 */
public class TestSpanishMatcher extends TestMatcherBase
{

    @Before
    public void setUp() throws Exception
    {
        corpusFolder += "/spanish";
        matcher = new SpanishMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String fromLine = "De: chuong nguyen [mailto:chuongnn.atlassian.cus1@gmail.com] ";
        String toLine = "Para: 'chuong nguyen'";
        String ccLine = "CC: 'chuong nguyen'";
        String dateLine = "Enviado el: Thursday, June 18, 2015 11:04 AM";
        String dateAtTopLine = "Enviado el: Thursday, June 18, 2015 11:04 AM De: ";
        String subjectLine = "Asunto: Outlook 2013 hola español 2";
        String subjectAtTopLine = "Asunto: Outlook 2013 hola español 2 De: ";

        testPatterns(fromLine, toLine, ccLine, dateLine, dateAtTopLine, subjectLine, subjectAtTopLine);
    }

    @Test
    public void testReplyEmailMatch() throws Exception
    {
        super.testReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCReplyEmailMatch() throws Exception
    {
        super.testWithCCReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameReplyEmailMatch() throws Exception
    {
        super.testLongFullnameReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentReplyEmailMatch() throws Exception
    {
        super.testMaliciousContentReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testForwardEmailMatch() throws Exception
    {
        super.testForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCForwardEmailMatch() throws Exception
    {
        super.testWithCCForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameForwardEmailMatch() throws Exception
    {
        super.testLongFullnameForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentForwardEmailMatch() throws Exception
    {
        super.testMaliciousContentForwardEmailMatch(corpusFolder);
    }
}
