package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Unit tests for each regex. <br/>
 * 2. Normal reply. <br/>
 * 3. With cc reply. <br/>
 * 4. With reply-to reply. <br/>
 * 5. With long fullname reply. <br/>
 * 6. With malicious content reply. <br/>
 * 7. Normal forward. <br/>
 * 8. With cc forward. <br/>
 * 9. With reply-to forward. <br/>
 * 10. With long fullname forward. <br/>
 * 11. With malicious forward. <br/>
 * </p>
 */
public class TestEnglishMatcher extends TestMatcherBase
{

    @Before
    public void setUp() throws Exception
    {
        corpusFolder += "/english";
        matcher = new EnglishMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String fromLine = "From: admin <chuongnn.atlassian.ad@gmail.com>";
        String toLine = "To: chuong nguyen <chuongnn.atlassian.cus1@gmail.com>";
        String replyToLine = "Reply-To: <chuongnn.atlassian.patricia@gmail.com>";
        String ccLine = "Cc: chuong nguyen <chuongnn.atlassian.cus1@gmail.com>";
        String dateLine = "Date: Tuesday, June 16, 2015 at 11:22";
        String dateAtTopLine = "Date: Tuesday, June 16, 2015 at 11:22 From: ";
        String subjectLine = "Subject: [jira@localhost] [DEVONE] request 4002 English [DEV-78]";
        String subjectAtTopLine = "Subject: [jira@localhost] [DEVONE] request 4002 English [DEV-78]From: ";

        testPatterns(fromLine, toLine, replyToLine, ccLine, dateLine, dateAtTopLine, subjectLine, subjectAtTopLine);
    }

    @Test
    public void testReplyEmailMatch() throws Exception
    {
        super.testReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCReplyEmailMatch() throws Exception
    {
        super.testWithCCReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testWithReplyToReplyEmailMatch() throws Exception
    {
        super.testWithReplyToReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameReplyEmailMatch() throws Exception
    {
        super.testLongFullnameReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentReplyEmailMatch() throws Exception
    {
        super.testMaliciousContentReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testForwardEmailMatch() throws Exception
    {
        super.testForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCForwardEmailMatch() throws Exception
    {
        super.testWithCCForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testWithReplyToForwardEmailMatch() throws Exception
    {
        super.testWithReplyToForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameForwardEmailMatch() throws Exception
    {
        super.testLongFullnameForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentForwardEmailMatch() throws Exception
    {
        super.testMaliciousContentForwardEmailMatch(corpusFolder);
    }
}
