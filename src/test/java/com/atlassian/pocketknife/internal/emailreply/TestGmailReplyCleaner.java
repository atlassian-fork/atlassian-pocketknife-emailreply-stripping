package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import org.junit.Test;

public class TestGmailReplyCleaner {
    private final EmailReplyCleaner gmailCleaner = new EmailReplyCleanerBuilder().gmailMatcher().build();

    @Test
    public void testGmailReplyEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_ENGLISH_TEXT);
    }

    @Test
    public void testGmailForwardEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_ENGLISH_TEXT);
    }

    @Test
    public void testGmailReplyEmptyEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testGmailForwardEmptyEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyEnglishQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_ENGLISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplyEnglish2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_ENGLISH_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailEnglish1LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GENERAL_1_LINE_TEXT);
    }

    @Test
    public void testGmailEmptyBody() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GENERAL_EMPTY_BODY);
    }

    @Test
    public void testGmail4LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GENERAL_4_LINE_TEXT);
    }

    @Test
    public void testGmail5LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GENERAL_5_LINE_TEXT);
    }

    @Test
    public void testGmailReplyFrenchText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_FRENCH_TEXT);
    }

    @Test
    public void testGmailReplyFrenchEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testGmailForwardFrenchText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_FRENCH_TEXT);
    }

    @Test
    public void testGmailForwardFrenchEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyFrenchQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_FRENCH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplyFrench2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_FRENCH_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailReplyGermanText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GERMAN_TEXT);
    }

    @Test
    public void testGmailReplyGermanEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyGermanQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GERMAN_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplyGerman2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GERMAN_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailForwardGermanText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_GERMAN_TEXT);
    }

    @Test
    public void testGmailForwardGermanEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyJapText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_JAP_TEXT);
    }

    @Test
    public void testGmailReplyJapEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_JAP_EMPTY_TEXT);
    }

    @Test
    public void testGmailForwardJapText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_JAP_TEXT);
    }

    @Test
    public void testGmailForwardJapEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_JAP_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyJapQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_JAP_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplyJap2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_JAP_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailReplySpanishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_SPANISH_TEXT);
    }

    @Test
    public void testGmailReplySpanishEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testGmailForwardSpanishText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_SPANISH_TEXT);
    }

    @Test
    public void testGmailForwardSpanishEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_FORWARD_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplySpanishQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_SPANISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplySpanish2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_SPANISH_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailReplyGenericText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GENERIC_TEXT);
    }

    @Test
    public void testGmailReplyGenericEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GENERIC_EMPTY_TEXT);
    }

    @Test
    public void testGmailReplyGeneric2LineAttributionText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GENERIC_2_LINE_ATTRIBUTION_TEXT);
    }

    @Test
    public void testGmailReplyGenericQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_GENERIC_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testGmailReplyStyling() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_STYLING);
    }


    @Test
    public void testGmailReplyStylingLists() throws Exception {
        EmailTestUtils.validateEmailStripping(gmailCleaner, EmailCorpus.GMAIL_REPLY_STYLING_LISTS);
    }
}
