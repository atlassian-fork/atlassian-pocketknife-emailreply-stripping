package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Unit tests for each regex. <br/>
 * 2. Language based normal reply. <br/>
 * 3. Language based with long fullname reply. <br/>
 * 4. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherPatternMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String EnglishLine = "On 6/11/15 10:52 AM, Chuong Nguyen wrote:";
        String FrenchLine = "Le 6/12/15 10:18 AM, chuong nguyen a Ã©crit :";
        String GermanLine = "Am 6/11/15 um 5:22 PM schrieb chuong nguyen:";
        String JapaneseLine = "On 6/12/15 10:50 AM, chuong nguyen wrote:";
        String SpanishLine = "El 6/12/15 a las 9:29 AM, chuong nguyen escribiÃ³:";
        String EnglishMilitaryTimeLine = "On 6/11/15 10:52, Chuong Nguyen wrote:";
        String FrenchMilitaryTimeLine = "Le 6/12/15 10:18, chuong nguyen a Ã©crit :";
        String GermanMilitaryTimeLine = "Am 6/11/15 um 5:22 schrieb chuong nguyen:";
        String JapaneseMilitaryTimeLine = "On 6/12/15 10:50, chuong nguyen wrote:";
        String SpanishMilitaryTimeLine = "El 6/12/15 a las 9:29, chuong nguyen escribiÃ³:";
        String EnglishQuotedLine = "> On 6/11/15 10:52 AM, Chuong Nguyen wrote:";
        String FrenchQuotedLine = "> Le 6/12/15 10:18 AM, chuong nguyen a Ã©crit :";
        String GermanQuotedLine = "> Am 6/11/15 um 5:22 PM schrieb chuong nguyen:";
        String JapaneseQuotedLine = "> On 6/12/15 10:50 AM, chuong nguyen wrote:";
        String SpanishQuotedLine = "> El 6/12/15 a las 9:29 AM, chuong nguyen escribiÃ³:";

        patternMatchedTest(((ReplyMatcher) matcher).ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN, EnglishLine);
        patternMatchedTest(((ReplyMatcher) matcher).ATTRIBUTION_LINE_FRENCH_PATTERN, FrenchLine);
        patternMatchedTest(((ReplyMatcher) matcher).ATTRIBUTION_LINE_GERMAN_PATTERN, GermanLine);
        patternMatchedTest(((ReplyMatcher) matcher).ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN, JapaneseLine);
        patternMatchedTest(((ReplyMatcher) matcher).ATTRIBUTION_LINE_SPANISH_PATTERN, SpanishLine);
        patternMatchedTest(
                ((ReplyMatcher) matcher).MILITARY_TIME_ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN,
                EnglishMilitaryTimeLine);
        patternMatchedTest(
                ((ReplyMatcher) matcher).MILITARY_TIME_ATTRIBUTION_LINE_FRENCH_PATTERN, FrenchMilitaryTimeLine);
        patternMatchedTest(
                ((ReplyMatcher) matcher).MILITARY_TIME_ATTRIBUTION_LINE_GERMAN_PATTERN, GermanMilitaryTimeLine);
        patternMatchedTest(
                ((ReplyMatcher) matcher).MILITARY_TIME_ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN,
                JapaneseMilitaryTimeLine);
        patternMatchedTest(
                ((ReplyMatcher) matcher).MILITARY_TIME_ATTRIBUTION_LINE_SPANISH_PATTERN, SpanishMilitaryTimeLine);
        patternMatchedTest(((ReplyMatcher) matcher).QUOTED_LINE_PATTERN, EnglishQuotedLine);
        patternMatchedTest(((ReplyMatcher) matcher).QUOTED_LINE_PATTERN, FrenchQuotedLine);
        patternMatchedTest(((ReplyMatcher) matcher).QUOTED_LINE_PATTERN, GermanQuotedLine);
        patternMatchedTest(((ReplyMatcher) matcher).QUOTED_LINE_PATTERN, JapaneseQuotedLine);
        patternMatchedTest(((ReplyMatcher) matcher).QUOTED_LINE_PATTERN, SpanishQuotedLine);
    }
}