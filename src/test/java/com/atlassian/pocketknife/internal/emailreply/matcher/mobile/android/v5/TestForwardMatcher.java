package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

public class TestForwardMatcher extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/mobile/android/v5";

    private final String EnglishForwardEmailFile = corpusFolder + "/english/ForwardEmail.eml";
    private final String StrippedEnglishForwardEmailFile = corpusFolder + "/english/stripped/ForwardEmail.eml";

    private final String GermanForwardEmailFile = corpusFolder + "/german/ForwardEmail.eml";
    private final String StrippedGermanForwardEmailFile = corpusFolder + "/german/stripped/ForwardEmail.eml";

    private final String SpanishForwardEmailFile = corpusFolder + "/spanish/ForwardEmail.eml";
    private final String StrippedSpanishForwardEmailFile = corpusFolder + "/spanish/stripped/ForwardEmail.eml";

    private final String FrenchForwardEmailFile = corpusFolder + "/french/ForwardEmail.eml";
    private final String StrippedFrenchForwardEmailFile = corpusFolder + "/french/stripped/ForwardEmail.eml";

    private final String JapaneseForwardEmailFile = corpusFolder + "/japanese/ForwardEmail.eml";
    private final String StrippedJapaneseForwardEmailFile = corpusFolder + "/japanese/stripped/ForwardEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ForwardMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String EnglishLine = " ---------- Forwarded message ----------";
        String FrenchLine = " ---------- Message transféré ----------";
        String GermanLine = "---------- Weitergeleitete Nachricht ----------";
        String JapaneseLine = " ---------- 転送メッセージ ----------";
        String SpanishLine = "---------- Mensaje reenviado ----------";

        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, SpanishLine);
    }

    @Test
    public void testEnglishForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishForwardEmailFile, EnglishForwardEmailFile);
    }

    @Test
    public void testGermanForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanForwardEmailFile, GermanForwardEmailFile);
    }

    @Test
    public void testSpanishForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedSpanishForwardEmailFile, SpanishForwardEmailFile);
    }

    @Test
    public void testFrenchForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchForwardEmailFile, FrenchForwardEmailFile);
    }

    @Test
    public void testJapaneseForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseForwardEmailFile, JapaneseForwardEmailFile);
    }
}
