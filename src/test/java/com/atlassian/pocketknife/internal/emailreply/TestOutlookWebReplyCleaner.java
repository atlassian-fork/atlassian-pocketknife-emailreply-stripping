package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookWebMatcher;
import com.google.common.collect.Lists;
import org.junit.Test;

public class TestOutlookWebReplyCleaner {
    private final EmailReplyCleaner outlookCleaner = new DefaultEmailReplyCleaner(
            Lists.newArrayList(
                    new OutlookWebMatcher()
            )
    );

    @Test
    public void testOutlookEnglish1LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.GENERAL_1_LINE_TEXT);
    }

    @Test
    public void testOutlookEmptyBody() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.GENERAL_EMPTY_BODY);
    }

    @Test
    public void testOutlook4LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.GENERAL_4_LINE_TEXT);
    }

    @Test
    public void testOutlook5LineText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.GENERAL_5_LINE_TEXT);
    }

    @Test
    public void testOutlookReplyEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_ENGLISH_TEXT);
    }

    @Test
    public void testOutlookReplyPlainEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_PLAIN_ENGLISH_TEXT);
    }

    @Test
    public void testOutlookReplyCcEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_CC_ENGLISH_TEXT);
    }

    @Test
    public void testOutlookReplyEnglishEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testOutlookForwardEnglishText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_FORWARD_ENGLISH_TEXT);
    }

    @Test
    public void testOutlookForwardEnglishEmptyText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_FORWARD_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testOutlookReplyEnglishQuotedPatternText() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_ENGLISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testOutlookReplyStyling() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_STYLING);
    }

    @Test
    public void testOutlookReplyStylingLists() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_REPLY_STYLING_LISTS);
    }

    @Test
    public void testOutlookWebReplyNoDefaultSignature() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_WEB_NO_DEFAULT_SIGNATURE);
    }

    @Test
    public void testOutlookWebReplyDefaultSignature() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_WEB_DEFAULT_SIGNATURE);
    }

    @Test
    public void testOutlookWebReplyDefaultSignatureWithSpaceAtEnd() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_WEB_DEFAULT_SIGNATURE_WITH_SPACE_AT_END);
    }

}
