package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import org.junit.Test;

public class TestIPadReplyMatcher
{
    private final EmailReplyCleaner ipadCleaner = new EmailReplyCleanerBuilder().ipadMatcher().build();

    @Test
    public void testIpadEnglish1LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.GENERAL_1_LINE_TEXT);
    }

    @Test
    public void testIpadEmptyBody() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.GENERAL_EMPTY_BODY);
    }

    @Test
    public void testIpad4LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.GENERAL_4_LINE_TEXT);
    }

    @Test
    public void testIpad5LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.GENERAL_5_LINE_TEXT);
    }

    @Test
    public void testIpadEnglishReplyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_ENGLISH_TEXT);
    }

    @Test
    public void testIpadEnglishReplyTextNoBlankLine() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_ENGLISH_TEXT_NO_BLANK_LINE);
    }

    @Test
    public void testIpadEnglishReplyEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testIpadEnglishReplyQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_ENGLISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIpadEnglishReplyWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_ENGLISH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadEnglishForwardText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_ENGLISH_TEXT);
    }

    @Test
    public void testIpadEnglishForwardEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testIpadEnglishForwardNoBlankLine() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_ENGLISH_NO_BLANK_LINE);
    }

    @Test
    public void testIpadEnglishForwardWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_ENGLISH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadFrenchReplyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_FRENCH_TEXT);
    }

    @Test
    public void testIpadFrenchReplyEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testIpadFrenchReplyQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_FRENCH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIpadFrenchReplyWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_FRENCH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadFrenchForwardText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_FRENCH_TEXT);
    }

    @Test
    public void testIpadFrenchForwardEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testIpadFrenchForwardWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_FRENCH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadGermanReplyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_GERMAN_TEXT);
    }

    @Test
    public void testIpadGermanReplyQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_GERMAN_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIpadGermanReplyEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testIpadGermanReplyWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_GERMAN_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadGermanForwardText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_GERMAN_TEXT);
    }

    @Test
    public void testIpadGermanForwardEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testIpadGermanForwardWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_GERMAN_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadReplySpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_SPANISH_TEXT);
    }

    @Test
    public void testIpadReplySpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testIpadReplySpanishQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_SPANISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIpadReplySpanishWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_SPANISH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadForwardSpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_SPANISH_TEXT);
    }

    @Test
    public void testIpadForwardSpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testIpadForwardSpanishWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_SPANISH_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadReplyJap() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_JAP_TEXT);
    }

    @Test
    public void testIpadReplyJapQuotedPattern() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_JAP_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIpadReplyJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_JAP_EMPTY_TEXT);
    }

    @Test
    public void testIpadReplyJapWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_JAP_WITHOUT_SIGNATURE);
    }

    @Test
    public void testIpadReplyJapWithoutSignatureCompact() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_REPLY_JAP_WITHOUT_SIGNATURE_COMPACT);
    }

    @Test
    public void testIpadForwardJapText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_JAP_TEXT);
    }

    @Test
    public void testIpadForwardJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_JAP_EMPTY_TEXT);
    }

    @Test
    public void testIpadForwardJapWithoutSignature() throws Exception
    {
        EmailTestUtils.validateEmailStripping(ipadCleaner, EmailCorpus.IPAD_FORWARD_JAP_WITHOUT_SIGNATURE);
    }
}
