package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.converters.wiki.HtmlToWikiTextConverter;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import com.atlassian.pocketknife.internal.emailreply.matcher.GmailReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.IPhoneReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.atlassian.pocketknife.internal.emailreply.matcher.YahooReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31.ForwardMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import javax.mail.Message;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestDefaultEmailReplyCleaner extends TestBase {

    @Test
    public void testNoDuplicateMatcherDespiteMultipleCall() {
        DefaultEmailReplyCleaner defaultEmailReplyCleaner = new DefaultEmailReplyCleaner(ImmutableList.of(new GmailReplyMatcher(), new GmailReplyMatcher()));

        final List<QuotedEmailMatcher> expectedMatchers;
        expectedMatchers = ImmutableList.of(new GmailReplyMatcher());

        assertThat(
                "must contain only 1 instance of Gmail matcher is in use",
                defaultEmailReplyCleaner.getMatchers(),
                equalTo(expectedMatchers));
    }

    @Test
    public void testMatchersAreAddedInOrder() {
        final QuotedEmailMatcher mock = mock(QuotedEmailMatcher.class);
        final QuotedEmailMatcher forward = new ForwardMatcher();

        DefaultEmailReplyCleaner defaultEmailReplyCleaner = new DefaultEmailReplyCleaner(ImmutableList.of(new GmailReplyMatcher(), new YahooReplyMatcher(), mock, new IPhoneReplyMatcher(), new GmailReplyMatcher(), forward));

        final List<QuotedEmailMatcher> expectedMatchers = ImmutableList.of(new GmailReplyMatcher(), new YahooReplyMatcher(), mock, new IPhoneReplyMatcher(), forward);
        assertThat(
                "matchers are recorded in order",
                defaultEmailReplyCleaner.getMatchers(),
                equalTo(expectedMatchers));
    }

    @Test
    public void testGmailTextReplyInBlockQuoteStripping() throws Exception {
        EmailReplyCleaner defaultEmailReplyCleaner = new EmailReplyCleanerBuilder().defaultMatchers().build();
        assertThat(defaultEmailReplyCleaner, IsInstanceOf.instanceOf(DefaultEmailReplyCleaner.class));

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailHtmlAndTextReplyInBlockQuote.eml");
        final EmailReplyCleaner.EmailCleanerResult result = defaultEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertThat(result.getBody(), is("Test\n" +
                "\n" +
                "Quote this\n" +
                ">"));

        assertThat(result.getRawBody(), is("Test\n" +
                "\n" +
                "Quote this\n" +
                ">"));

        assertThat(result.getOriginalBody(), is("Test\n" +
                "\n" +
                "Quote this\n" +
                ">\n" +
                "> On 27 January 2016 at 01:43, Matthew McMahon <mmcmahon@atlassian.com>\n" +
                "> wrote:\n" +
                "> *This* is a very *simple* *formatting test*\n" +
                ">\n" +
                ">    - *sdfsdf*\n" +
                ">\n" +
                ">\n" +
                ">    - *sdfsdfsdf*\n" +
                ">\n" +
                ">\n" +
                ">\n" +
                ">    1. sdfsdf\n" +
                ">\n" +
                ">\n" +
                ">    1. *sdfsdf*  sdfsdf   *sdf sd *\n" +
                ">\n" +
                "> That will do!\n"));
    }

}
