package com.atlassian.pocketknife.internal.emailreply.util;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestEmailTextReader
{
    @Test
    public void testReadEmptyString()
    {
        final EmailTextReader reader = new EmailTextReader("");
        final List<String> blocks = reader.readNextBlock();

        Assert.assertTrue(blocks.isEmpty());
    }

    @Test
    public void testReadNullString()
    {
        final EmailTextReader reader = new EmailTextReader(null);
        final List<String> blocks = reader.readNextBlock();

        Assert.assertTrue(blocks.isEmpty());
    }

    @Test
    public void testRead1LineString()
    {
        final EmailTextReader reader = new EmailTextReader("simple string");

        Assert.assertEquals(Lists.newArrayList("simple string"), reader.readNextBlock());
    }

    @Test
    public void testReadMultiLineText()
    {
        final EmailTextReader reader = new EmailTextReader("line1\nline2\nline3\nline4");

        Assert.assertEquals(Lists.newArrayList("line1", "line2", "line3", "line4"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line2", "line3", "line4"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line3", "line4"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line4"), reader.readNextBlock());
        Assert.assertEquals(Lists.<String>newArrayListWithExpectedSize(0), reader.readNextBlock());
    }

    @Test
    public void testReadLinesMixedWithMultiNewLineChars()
    {
        final EmailTextReader reader = new EmailTextReader("line1\n\n\n\nline2\nline3\nline4\rline5\r\nline6");

        Assert.assertEquals(Lists.newArrayList("line1", "", "", "", "line2"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("", "", "", "line2", "line3"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("", "", "line2", "line3", "line4"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("", "line2", "line3", "line4", "line5"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line2", "line3", "line4", "line5", "line6"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line3", "line4", "line5", "line6"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line4", "line5", "line6"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line5", "line6"), reader.readNextBlock());
        Assert.assertEquals(Lists.newArrayList("line6"), reader.readNextBlock());
        Assert.assertEquals(Lists.<String>newArrayListWithExpectedSize(0), reader.readNextBlock());
    }
}