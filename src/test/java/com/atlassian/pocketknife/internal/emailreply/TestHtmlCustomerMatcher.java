package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.converters.wiki.HtmlToWikiTextConverter;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.mail.Message;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.Collections.emptyList;
import static org.apache.commons.lang.StringUtils.strip;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TestHtmlCustomerMatcher extends TestBase {

    @Test
    public void testHtmlFormattingReply() throws Exception {
        final EmailReplyCleaner customHtmlEmailReplyCleaner = new EmailReplyCleanerBuilder().customMatchers(ImmutableList.of(new MarkerBasedMatcher())).preferHtml(new HtmlToWikiTextConverter(emptyList())).build();

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/HtmlFormattingReply.eml");

        EmailReplyCleaner.EmailCleanerResult result = customHtmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        // for this, we can detect that a forwarded message can be stripped
        assertEquals("{color:#ff9900}eragfaegfsefdg{color}\n" +
                "{color:#ff9900}easgvaesf;dgofjbkcw;asgfd{color}\n" +
                "* wadosfjbleaisgbvaegreagagdfadbf\n" +
                "* adfbaebd\n" +
                "# adfgbeabdf\n" +
                "\n" +
                " _adbaefgsaegfs +aefbdsdfbsdfb *dfbsgbdzbdb*+_\n" +
                "\n" +
                "On 30 November 2016 at 07:23, incoming mail project <[jira.ams.mail@gmail.com|mailto:jira.ams.mail@gmail.com]> wrote:\n" +
                "\n" +
                "{quote}{quote}", result.getBody());

        final EmailReplyCleaner defaultEmailReplyCleaner = new EmailReplyCleanerBuilder().customMatchers(ImmutableList.of(new MarkerBasedMatcher())).build();

        result = defaultEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);
        // for this, we can detect that a forwarded message can be stripped
        assertEquals("eragfaegfsefdg\n" +
                "easgvaesf;dgofjbkcw;asgfd\n" +
                "\n" +
                "   - wadosfjbleaisgbvaegreagagdfadbf\n" +
                "   - adfbaebd\n" +
                "\n" +
                "\n" +
                "   1. adfgbeabdf\n" +
                "\n" +
                "*adbaefgsaegfsaefbdsdfbsdfbdfbsgbdzbdb*\n" +
                "\n" +
                "On 30 November 2016 at 07:23, incoming mail project <jira.ams.mail@gmail.com\n" +
                "> wrote:", result.getBody());
    }

    @Test
    public void testHtmlMarkersReply() throws Exception {
        final EmailReplyCleaner htmlEmailReplyCleaner = new EmailReplyCleanerBuilder().defaultMatchers().customMatchers(ImmutableList.of(new MarkerBasedMatcher())).preferHtml(new HtmlToWikiTextConverter(emptyList())).build();

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/html_markers.eml");

        EmailReplyCleaner.EmailCleanerResult result = htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        // for this, we can detect that a forwarded message can be stripped
        assertEquals("{color:#000000}\n" +
                "\n" +
                "Testing \n" +
                "\n" +
                "\n" +
                "  \n" +
                " {color}{color:#000000} \n" +
                "\n" +
                " \n" +
                " --\n" +
                " James Hook, CSM \n" +
                " Agile Project Manager{color} \n" +
                " 1464395258342_Yrsu{color:#000000}\n" +
                "  \n" +
                " Cell: 111-123-1284 \n" +
                " Office: 111-456-3109{color} \n" +
                " [{color:#000000}eclowdown.com{color}|http://www.eclowdown.com/]{color:#000000}{color} \n" +
                " [{color:#000000}linkedin.com/in/scrummasterJamesHook{color}|https://www.linkedin.com/in/scrummasterJamesHook]{color:#000000}{color} \n" +
                "\n" +
                " \n" +
                "\n" +
                " {color:#000000}{color}{color:#000000}{color}\n" +
                "\n" +
                "{color:#000000}{color}", result.getBody());

        final EmailReplyCleaner customOnlyHtmlEmailReplyCleaner = new EmailReplyCleanerBuilder().customMatchers(ImmutableList.of(new MarkerBasedMatcher())).preferHtml(new HtmlToWikiTextConverter(emptyList())).build();

        result = customOnlyHtmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);
        // for this, we can detect that a forwarded message can be stripped
        assertEquals("{color:#000000}\n" +
                "\n" +
                "Testing \n" +
                "\n" +
                "\n" +
                "  \n" +
                " {color}{color:#000000} \n" +
                "\n" +
                " \n" +
                " --\n" +
                " James Hook, CSM \n" +
                " Agile Project Manager{color} \n" +
                " 1464395258342_Yrsu{color:#000000}\n" +
                "  \n" +
                " Cell: 111-123-1284 \n" +
                " Office: 111-456-3109{color} \n" +
                " [{color:#000000}eclowdown.com{color}|http://www.eclowdown.com/]{color:#000000}{color} \n" +
                " [{color:#000000}linkedin.com/in/scrummasterJamesHook{color}|https://www.linkedin.com/in/scrummasterJamesHook]{color:#000000}{color} \n" +
                "\n" +
                " \n" +
                "\n" +
                " {color:#000000}  \n" +
                " \n" +
                " \n" +
                "{color}{color:#000000} \n" +
                "{color}\n" +
                "----\n" +
                "{color:#000000} \n" +
                "{color}{color:#000000} *From:* Over This Coach Helpdesk <jira@eclowdown.atlassian.net>\n" +
                "  *Sent:* Tuesday, December 13, 2016 3:55 PM\n" +
                "  *To:* James Hook\n" +
                "  *Subject:* UYTPOKG-52 Test of default sharing permission on email channel{color}{color:#000000}" +
                "{color}   {color:#000000}{color}", result.getBody());
    }

    @Test
    public void testStrippingWorksOnWiki() throws Exception {
        final Message message = EmailCorpus.OUTLOOK_WEB_DEFAULT_SIGNATURE.toMimeMessage();

        EmailReplyCleaner htmlEmailReplyCleaner = new EmailReplyCleanerBuilder().outlookDesktopMatcher().preferHtml(new HtmlToWikiTextConverter(emptyList())).build();
        assertThat(htmlEmailReplyCleaner, IsInstanceOf.instanceOf(HtmlEmailReplyCleaner.class));

        final EmailReplyCleaner.EmailCleanerResult emailCleanerResult = htmlEmailReplyCleaner.cleanQuotedEmail(message);

        assertThat("The email will not have been stripped, as the body is same as original", strip(emailCleanerResult.getBody()), is(strip(emailCleanerResult.getOriginalBody())));

        htmlEmailReplyCleaner = new EmailReplyCleanerBuilder().outlookDesktopMatcher().customMatchers(ImmutableList.of(new MarkerBasedMatcher())).preferHtml(new HtmlToWikiTextConverter(emptyList())).build();

        final EmailReplyCleaner.EmailCleanerResult emailCleanerResultWithCustom = htmlEmailReplyCleaner.cleanQuotedEmail(message);

        assertThat("Body should be stripped at the custom marker now", emailCleanerResultWithCustom.getBody(), is("{color:#000000}\n" +
                "\n" +
                "I'm the kliou_test@outlook.com account, leaving a comment with Strip Quotes and HTML Parsing on.\n" +
                "  \n" +
                "\n" +
                "\n" +
                "  \n" +
                " \n" +
                "{color}{color:#000000} Sent from{color} [{color:#000000}Outlook{color}|http://aka.ms/weboutlook]{color:#000000}{color}{color:#000000}{color} \n" +
                "----\n" +
                " \n" +
                "{color:#000000} *From:* SDTEST <kliouatlassmtp@gmail.com>\n" +
                "  *Sent:* Tuesday, January 10, 2017 6:05:01 PM\n" +
                "  *To:* kliou_test@outlook.com\n" +
                "  *Subject:* [JIRA7.2.3] Kevin added you to request #SDTEST-9: Testing Mail Quote Stripping (new){color}"));
    }

    private static class MarkerBasedMatcher implements QuotedEmailMatcher {
        private static final Set<String> quotedCharacters = ImmutableSet.of(">", "|");
        private static final Set<String> markers = ImmutableSet.of("\u2014Write replies above\u2014", "\u2014-\u2014-\u2014-\u2014");

        @Override
        public boolean isQuotedEmail(@Nonnull List<String> textBlock) {
            if (textBlock == null || textBlock.size() == 0) {
                return false;
            }

            return isCustomMarkerLine(textBlock.get(0)) ||
                    isQuotedCustomMarkerLine(textBlock.get(0)) ||
                    isWikiColourLine(textBlock.get(0));
        }

        private boolean isCustomMarkerLine(String line) {
            String trimmedLine = line.trim();
            for (String marker : markers) {
                if (trimmedLine.equalsIgnoreCase(marker)) {
                    return true;
                }
            }
            return false;
        }

        private boolean isQuotedCustomMarkerLine(String line) {
            for (String marker : markers) {
                if (line.trim().endsWith(marker) && isQuotedLine(line)) {
                    return true;
                }
            }
            return false;
        }

        private boolean isWikiColourLine(String line) {
            final String trim = line.trim();
            for (String marker : markers) {
                final Pattern compile = Pattern.compile(RegexUtils.buildRegexFromSkeleton("^(\\{color:.*\\})?\\s*${0}\\s*(\\{color\\})?$", marker));
                if (RegexUtils.match(compile, trim)) {
                    return true;
                }
            }
            return false;
        }

        private boolean isQuotedLine(String line) {
            for (String quotedCharacter : quotedCharacters) {
                if (line.startsWith(quotedCharacter)) {
                    return true;
                }
            }
            return false;
        }

    }

}
