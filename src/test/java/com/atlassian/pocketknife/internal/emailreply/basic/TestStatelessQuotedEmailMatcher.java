package com.atlassian.pocketknife.internal.emailreply.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;

public class TestStatelessQuotedEmailMatcher
{
    private final StatelessQuotedEmailMatcher instanceA1 = new MatcherA();
    private final StatelessQuotedEmailMatcher instanceA2 = new MatcherA();
    private final StatelessQuotedEmailMatcher instanceB1 = new MatcherB();

    @Test
    public void testHashCode()
    {
        Assert.assertThat("same instance must have same hash code", instanceA1.hashCode(), equalTo(instanceA2.hashCode()));
        Assert.assertThat("diff instances of same class must have same hash code", instanceA1.hashCode(), equalTo(instanceA2.hashCode()));
        Assert.assertThat("diff instances of diff classes must have diff hash code", instanceA1.hashCode(), not(equalTo(instanceB1.hashCode())));
    }

    @Test
    public void testEqual()
    {
        Assert.assertThat("never equals to null", instanceA1, not(equalTo(null)));
        Assert.assertThat("never equals to null", null, not(equalTo(instanceA1)));

        Assert.assertThat("equal is reflexive", instanceA1, equalTo(instanceA1));

        Assert.assertThat("equal is symmetric", instanceA1, equalTo(instanceA2));
        Assert.assertThat("equal is symmetric", instanceA2, equalTo(instanceA1));

        Assert.assertThat("diff instances of same class must be equal", instanceA1, equalTo(instanceA2));
        Assert.assertThat("diff instances of diff classes must not be equal", instanceA1, not(equalTo(instanceB1)));
    }

    private class MatcherA extends StatelessQuotedEmailMatcher {
        @Override
        public boolean isQuotedEmail(List<String> textBlock)
        {
            return false;
        }
    }

    private class MatcherB extends StatelessQuotedEmailMatcher {
        @Override
        public boolean isQuotedEmail(List<String> textBlock)
        {
            return false;
        }
    }
}
