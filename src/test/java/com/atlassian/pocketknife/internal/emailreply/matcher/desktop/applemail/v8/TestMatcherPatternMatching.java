package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.v8;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

public class TestMatcherPatternMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/applemail/v8";

    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String EnglishReplyAttributionLine = "> On Jun 9, 2015, at 2:55 PM, customer <chuongnn.atlassian.ad@gmail.com> wrote:";
        String FrenchReplyAttributionLine = "> Le 12 juin 2015 à 5:25 PM, Chuong Nguyen <cnguyen@atlassian.com> a écrit :";
        String GermanReplyAttributionLine = "> Am 12.06.2015 um 5:15 PM schrieb Chuong Nguyen <cnguyen@atlassian.com>:";
        String JapaneseReplyAttributionLine = "> 2015/06/12 5:50 PM、chuong nguyen <cnguyen@atlassian.com> のメール：";
        String SpanishReplyAttributionLine = "> El 12/6/2015, a las 5:45 PM, chuong nguyen <cnguyen@atlassian.com> escribió:";
        String EnglishMilitaryTimeReplyAttributionLine = "> On Jun 9, 2015, at 2:55, customer <chuongnn.atlassian.ad@gmail.com> wrote:";
        String FrenchMilitaryTimeReplyAttributionLine = "> Le 12 juin 2015 à 15:25, Chuong Nguyen <cnguyen@atlassian.com> a écrit :";
        String GermanMilitaryTimeReplyAttributionLine = "> Am 12.06.2015 um 15:15 schrieb Chuong Nguyen <cnguyen@atlassian.com>:";
        String JapaneseMilitaryTimeReplyAttributionLine = "> 2015/06/12 15:50、chuong nguyen <cnguyen@atlassian.com> のメール：";
        String SpanishMilitaryTimeReplyAttributionLine = "> El 12/6/2015, a las 15:45, chuong nguyen <cnguyen@atlassian.com> escribió:";
        String EnglishForwardAttributionLine = "> Begin forwarded message:";
        String FrenchForwardAttributionLine = "> Début du message réexpédié :";
        String GermanForwardAttributionLine = "> Anfang der weitergeleiteten Nachricht:";
        String JapaneseForwardAttributionLine = "> 転送されたメッセージ：";
        String SpanishForwardAttributionLine = "> Inicio del mensaje reenviado:";

        Matcher matcherInstance = (Matcher) matcher;

        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_ENGLISH_REPLY_PATTERN, EnglishReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_FRENCH_REPLY_PATTERN, FrenchReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_GERMAN_REPLY_PATTERN, GermanReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_JAPANESE_REPLY_PATTERN, JapaneseReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_SPANISH_REPLY_PATTERN, SpanishReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_REPLY_PATTERN,
                EnglishMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_REPLY_PATTERN,
                FrenchMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_REPLY_PATTERN,
                GermanMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_REPLY_PATTERN,
                JapaneseMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_REPLY_PATTERN,
                SpanishMilitaryTimeReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_ENGLISH_FORWARD_PATTERN, EnglishForwardAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_FRENCH_FORWARD_PATTERN, FrenchForwardAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_GERMAN_FORWARD_PATTERN, GermanForwardAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_JAPANESE_FORWARD_PATTERN, JapaneseForwardAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_SPANISH_FORWARD_PATTERN, SpanishForwardAttributionLine);
    }
}