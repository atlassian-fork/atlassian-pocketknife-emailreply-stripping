package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherEnglishMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/mobile/android/v5/english";

    private final String ReplyEmailFile = corpusFolder + "/ReplyEmail.eml";
    private final String StrippedReplyEmailFile = corpusFolder + "/stripped/ReplyEmail.eml";

    private final String LongFullnameReplyEmailFile = corpusFolder + "/LongFullnameReplyEmail.eml";
    private final String StrippedLongFullnameReplyEmailFile = corpusFolder + "/stripped/LongFullnameReplyEmail.eml";

    private final String MaliciousContentReplyEmailFile = corpusFolder + "/MaliciousContentReplyEmail.eml";
    private final String StrippedMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/MaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedReplyEmailFile, ReplyEmailFile);
    }

    @Test
    public void testLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedLongFullnameReplyEmailFile, LongFullnameReplyEmailFile);
    }

    @Test
    public void testMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedMaliciousContentReplyEmailFile, MaliciousContentReplyEmailFile);
    }
}
