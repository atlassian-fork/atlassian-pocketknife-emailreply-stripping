package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Unit tests for each regex. <br/>
 * 2. Normal reply. <br/>
 * 3. With cc reply. <br/>
 * 4. With reply-to reply. <br/>
 * 5. With long fullname reply. <br/>
 * 6. With malicious content reply. <br/>
 * 7. Normal forward. <br/>
 * 8. With cc forward. <br/>
 * 9. With reply-to forward. <br/>
 * 10. With long fullname forward. <br/>
 * 11. With malicious forward. <br/>
 * </p>
 */
public class TestSpanishMatcher extends TestMatcherBase
{

    @Before
    public void setUp() throws Exception
    {
        corpusFolder += "/spanish";
        matcher = new SpanishMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String fromLine = "De: aaa <chuongnn.atlassian.cus1@gmail.com>";
        String toLine = "Para: aaa <chuongnn.atlassian.cus1@gmail.com>";
        String replyToLine = "Responder a: <chuongnn.atlassian.patricia@gmail.com>";
        String ccLine = "CC: chuong nguyen <chuongnn.atlassian.cus1@gmail.com>";
        String dateLine = "Fecha: Monday, June 15, 2015 at 21:45";
        String dateAtTopLine = "Fecha: Monday, June 15, 2015 at 21:45De: ";
        String subjectLine = "Asunto: FW: Outlook 2011 hola imperio español 1";
        String subjectAtTopLine = "Asunto: FW: Outlook 2011 hola imperio español 1De: ";

        testPatterns(fromLine, toLine, replyToLine, ccLine, dateLine, dateAtTopLine, subjectLine, subjectAtTopLine);
    }

    @Test
    public void testReplyEmailMatch() throws Exception
    {
        super.testReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCReplyEmailMatch() throws Exception
    {
        super.testWithCCReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testWithReplyToReplyEmailMatch() throws Exception
    {
        super.testWithReplyToReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameReplyEmailMatch() throws Exception
    {
        super.testLongFullnameReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentReplyEmailMatch() throws Exception
    {
        super.testMaliciousContentReplyEmailMatch(corpusFolder);
    }

    @Test
    public void testForwardEmailMatch() throws Exception
    {
        super.testForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testWithCCForwardEmailMatch() throws Exception
    {
        super.testWithCCForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testWithReplyToForwardEmailMatch() throws Exception
    {
        super.testWithReplyToForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testLongFullnameForwardEmailMatch() throws Exception
    {
        super.testLongFullnameForwardEmailMatch(corpusFolder);
    }

    @Test
    public void testMaliciousContentForwardEmailMatch() throws Exception
    {
        super.testMaliciousContentForwardEmailMatch(corpusFolder);
    }
}
