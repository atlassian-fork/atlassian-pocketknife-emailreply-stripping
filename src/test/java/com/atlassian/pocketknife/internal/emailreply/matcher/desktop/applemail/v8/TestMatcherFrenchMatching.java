package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.v8;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * 4. Language based normal forward. <br/>
 * 5. Language based with malicious forward. <br/>
 * </p>
 */
public class TestMatcherFrenchMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/applemail/v8";

    private final String FrenchReplyEmailFile = corpusFolder + "/FrenchReplyEmail.eml";
    private final String StrippedFrenchReplyEmailFile = corpusFolder + "/stripped/FrenchReplyEmail.eml";

    private final String FrenchLongFullnameReplyEmailFile = corpusFolder + "/FrenchLongFullnameReplyEmail.eml";
    private final String StrippedFrenchLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/FrenchLongFullnameReplyEmail.eml";

    private final String FrenchMaliciousContentReplyEmailFile = corpusFolder + "/FrenchMaliciousContentReplyEmail.eml";
    private final String StrippedFrenchMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/FrenchMaliciousContentReplyEmail.eml";

    private final String FrenchForwardEmailFile = corpusFolder + "/FrenchForwardEmail.eml";
    private final String StrippedFrenchForwardEmailFile = corpusFolder + "/stripped/FrenchForwardEmail.eml";

    private final String FrenchMaliciousContentForwardEmailFile =
            corpusFolder + "/FrenchMaliciousContentForwardEmail.eml";
    private final String StrippedFrenchMaliciousContentForwardEmailFile =
            corpusFolder + "/stripped/FrenchMaliciousContentForwardEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testFrenchReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchReplyEmailFile, FrenchReplyEmailFile);
    }

    @Test
    public void testFrenchLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchLongFullnameReplyEmailFile, FrenchLongFullnameReplyEmailFile);
    }

    @Test
    public void testFrenchMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchMaliciousContentReplyEmailFile, FrenchMaliciousContentReplyEmailFile);
    }

    @Test
    public void testFrenchForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchForwardEmailFile, FrenchForwardEmailFile);
    }

    @Test
    public void testFrenchMaliciousContentForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchMaliciousContentForwardEmailFile, FrenchMaliciousContentForwardEmailFile);
    }

}
