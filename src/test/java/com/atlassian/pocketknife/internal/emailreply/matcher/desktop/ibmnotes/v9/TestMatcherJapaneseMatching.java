package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.v9;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * 4. Language based normal forward. <br/>
 * 5. Language based with long fullname forward. <br/>
 * 6. Language based with malicious forward. <br/>
 * </p>
 */
public class TestMatcherJapaneseMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/ibmnotes/v9";

    private final String JapaneseReplyEmailFile = corpusFolder + "/JapaneseReplyEmail.eml";
    private final String StrippedJapaneseReplyEmailFile = corpusFolder + "/stripped/JapaneseReplyEmail.eml";

    private final String JapaneseLongFullnameReplyEmailFile = corpusFolder + "/JapaneseLongFullnameReplyEmail.eml";
    private final String StrippedJapaneseLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/JapaneseLongFullnameReplyEmail.eml";

    private final String JapaneseMaliciousContentReplyEmailFile =
            corpusFolder + "/JapaneseMaliciousContentReplyEmail.eml";
    private final String StrippedJapaneseMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/JapaneseMaliciousContentReplyEmail.eml";

    private final String JapaneseForwardEmailFile = corpusFolder + "/JapaneseForwardEmail.eml";
    private final String StrippedJapaneseForwardEmailFile = corpusFolder + "/stripped/JapaneseForwardEmail.eml";

    private final String JapaneseLongFullnameForwardEmailFile = corpusFolder + "/JapaneseLongFullnameForwardEmail.eml";
    private final String StrippedJapaneseLongFullnameForwardEmailFile =
            corpusFolder + "/stripped/JapaneseLongFullnameForwardEmail.eml";

    private final String JapaneseMaliciousContentForwardEmailFile =
            corpusFolder + "/JapaneseMaliciousContentForwardEmail.eml";
    private final String StrippedJapaneseMaliciousContentForwardEmailFile =
            corpusFolder + "/stripped/JapaneseMaliciousContentForwardEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testJapaneseReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseReplyEmailFile, JapaneseReplyEmailFile);
    }

    @Test
    public void testJapaneseLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseLongFullnameReplyEmailFile, JapaneseLongFullnameReplyEmailFile);
    }

    @Test
    public void testJapaneseMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseMaliciousContentReplyEmailFile, JapaneseMaliciousContentReplyEmailFile);
    }

    @Test
    public void testJapaneseForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseForwardEmailFile, JapaneseForwardEmailFile);
    }

    @Test
    public void testJapaneseLongFullnameForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseLongFullnameForwardEmailFile, JapaneseLongFullnameForwardEmailFile);
    }

    @Test
    public void testJapaneseMaliciousContentForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseMaliciousContentForwardEmailFile, JapaneseMaliciousContentForwardEmailFile);
    }
}
