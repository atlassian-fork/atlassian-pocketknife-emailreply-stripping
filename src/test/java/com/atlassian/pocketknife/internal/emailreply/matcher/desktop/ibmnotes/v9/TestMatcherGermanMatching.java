package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.v9;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * 4. Language based normal forward. <br/>
 * 5. Language based with long fullname forward. <br/>
 * 6. Language based with malicious forward. <br/>
 * </p>
 */
public class TestMatcherGermanMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/ibmnotes/v9";

    private final String GermanReplyEmailFile = corpusFolder + "/GermanReplyEmail.eml";
    private final String StrippedGermanReplyEmailFile = corpusFolder + "/stripped/GermanReplyEmail.eml";

    private final String GermanLongFullnameReplyEmailFile = corpusFolder + "/GermanLongFullnameReplyEmail.eml";
    private final String StrippedGermanLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/GermanLongFullnameReplyEmail.eml";

    private final String GermanMaliciousContentReplyEmailFile = corpusFolder + "/GermanMaliciousContentReplyEmail.eml";
    private final String StrippedGermanMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/GermanMaliciousContentReplyEmail.eml";

    private final String GermanForwardEmailFile = corpusFolder + "/GermanForwardEmail.eml";
    private final String StrippedGermanForwardEmailFile = corpusFolder + "/stripped/GermanForwardEmail.eml";

    private final String GermanLongFullnameForwardEmailFile = corpusFolder + "/GermanLongFullnameForwardEmail.eml";
    private final String StrippedGermanLongFullnameForwardEmailFile =
            corpusFolder + "/stripped/GermanLongFullnameForwardEmail.eml";

    private final String GermanMaliciousContentForwardEmailFile =
            corpusFolder + "/GermanMaliciousContentForwardEmail.eml";
    private final String StrippedGermanMaliciousContentForwardEmailFile =
            corpusFolder + "/stripped/GermanMaliciousContentForwardEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testGermanReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanReplyEmailFile, GermanReplyEmailFile);
    }

    @Test
    public void testGermanLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanLongFullnameReplyEmailFile, GermanLongFullnameReplyEmailFile);
    }

    @Test
    public void testGermanMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanMaliciousContentReplyEmailFile, GermanMaliciousContentReplyEmailFile);
    }

    @Test
    public void testGermanForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanForwardEmailFile, GermanForwardEmailFile);
    }

    @Test
    public void testGermanLongFullnameForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanLongFullnameForwardEmailFile, GermanLongFullnameForwardEmailFile);
    }

    @Test
    public void testGermanMaliciousContentForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanMaliciousContentForwardEmailFile, GermanMaliciousContentForwardEmailFile);
    }
}
