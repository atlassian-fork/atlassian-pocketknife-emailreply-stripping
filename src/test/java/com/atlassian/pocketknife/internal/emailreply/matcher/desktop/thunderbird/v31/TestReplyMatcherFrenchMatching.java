package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherFrenchMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String FrenchReplyEmailFile = corpusFolder + "/FrenchReplyEmail.eml";
    private final String StrippedFrenchReplyEmailFile = corpusFolder + "/stripped/FrenchReplyEmail.eml";

    private final String FrenchLongFullnameReplyEmailFile = corpusFolder + "/FrenchLongFullnameReplyEmail.eml";
    private final String StrippedFrenchLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/FrenchLongFullnameReplyEmail.eml";

    private final String FrenchMaliciousContentReplyEmailFile = corpusFolder + "/FrenchMaliciousContentReplyEmail.eml";
    private final String StrippedFrenchMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/FrenchMaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testFrenchReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchReplyEmailFile, FrenchReplyEmailFile);
    }

    @Test
    public void testFrenchLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchLongFullnameReplyEmailFile, FrenchLongFullnameReplyEmailFile);
    }

    @Test
    public void testFrenchMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedFrenchMaliciousContentReplyEmailFile, FrenchMaliciousContentReplyEmailFile);
    }
}
