package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;

public class TestMatcherBase extends TestBase
{
    private final String ReplyEmailFile = "/ReplyEmail.eml";
    private final String StrippedReplyEmailFile = "/stripped/ReplyEmail.eml";

    private final String WithCCReplyEmailFile = "/WithCCReplyEmail.eml";
    private final String StrippedWithCCReplyEmailFile = "/stripped/WithCCReplyEmail.eml";

    private final String WithReplyToReplyEmailFile = "/WithReplyToReplyEmail.eml";
    private final String StrippedWithReplyToReplyEmailFile = "/stripped/WithReplyToReplyEmail.eml";

    private final String LongFullnameReplyEmailFile = "/LongFullnameReplyEmail.eml";
    private final String StrippedLongFullnameReplyEmailFile = "/stripped/LongFullnameReplyEmail.eml";

    private final String MaliciousContentReplyEmailFile = "/MaliciousContentReplyEmail.eml";
    private final String StrippedMaliciousContentReplyEmailFile = "/stripped/MaliciousContentReplyEmail.eml";

    private final String ForwardEmailFile = "/ForwardEmail.eml";
    private final String StrippedForwardEmailFile = "/stripped/ForwardEmail.eml";

    private final String WithCCForwardEmailFile = "/WithCCForwardEmail.eml";
    private final String StrippedWithCCForwardEmailFile = "/stripped/WithCCForwardEmail.eml";

    private final String WithReplyToForwardEmailFile = "/WithReplyToForwardEmail.eml";
    private final String StrippedWithReplyToForwardEmailFile = "/stripped/WithReplyToForwardEmail.eml";

    private final String LongFullnameForwardEmailFile = "/LongFullnameForwardEmail.eml";
    private final String StrippedLongFullnameForwardEmailFile = "/stripped/LongFullnameForwardEmail.eml";

    private final String MaliciousContentForwardEmailFile = "/MaliciousContentForwardEmail.eml";
    private final String StrippedMaliciousContentForwardEmailFile = "/stripped/MaliciousContentForwardEmail.eml";

    protected String corpusFolder = getDefaultCorpusFolder() + "/desktop/outlook/v2011";

    protected void testPatterns(String fromLine,
            String toLine,
            String replyToLine,
            String ccLine,
            String dateLine,
            String dateAtTopLine,
            String subjectLine,
            String subjectAtTopLine) throws Exception
    {
        BaseMatcher.MarkerDetector markerDetector = ((BaseMatcher) matcher).getMarkerDetector();

        patternMatchedTest(markerDetector.FROM_PATTERN, fromLine);
        patternMatchedTest(markerDetector.TO_PATTERN, toLine);
        patternMatchedTest(markerDetector.REPLY_TO_PATTERN, replyToLine);
        patternMatchedTest(markerDetector.CC_PATTERN, ccLine);
        patternMatchedTest(markerDetector.DATE_AT_TOP_PATTERN, dateAtTopLine);
        patternUnMatchedTest(markerDetector.DATE_AT_TOP_PATTERN, dateLine);
        patternMatchedTest(markerDetector.DATE_PATTERN, dateLine);
        patternMatchedTest(markerDetector.SUBJECT_AT_TOP_PATTERN, subjectAtTopLine);
        patternUnMatchedTest(markerDetector.SUBJECT_AT_TOP_PATTERN, subjectLine);
        patternMatchedTest(markerDetector.SUBJECT_PATTERN, subjectLine);
    }

    protected void testReplyEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedReplyEmailFile, corpusFolder + ReplyEmailFile);
    }

    protected void testWithCCReplyEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedWithCCReplyEmailFile, corpusFolder + WithCCReplyEmailFile);
    }

    protected void testWithReplyToReplyEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedWithReplyToReplyEmailFile, corpusFolder + WithReplyToReplyEmailFile);
    }

    protected void testLongFullnameReplyEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedLongFullnameReplyEmailFile, corpusFolder + LongFullnameReplyEmailFile);
    }

    protected void testMaliciousContentReplyEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(
                corpusFolder + StrippedMaliciousContentReplyEmailFile, corpusFolder + MaliciousContentReplyEmailFile);
    }

    protected void testForwardEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedForwardEmailFile, corpusFolder + ForwardEmailFile);
    }

    protected void testWithCCForwardEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedWithCCForwardEmailFile, corpusFolder + WithCCForwardEmailFile);
    }

    protected void testWithReplyToForwardEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedWithReplyToForwardEmailFile, corpusFolder + WithReplyToForwardEmailFile);
    }

    protected void testLongFullnameForwardEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(corpusFolder + StrippedLongFullnameForwardEmailFile, corpusFolder + LongFullnameForwardEmailFile);
    }

    protected void testMaliciousContentForwardEmailMatch(String corpusFolder) throws Exception
    {
        strippingTest(
                corpusFolder + StrippedMaliciousContentForwardEmailFile,
                corpusFolder + MaliciousContentForwardEmailFile);
    }
}
