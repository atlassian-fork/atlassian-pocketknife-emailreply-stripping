package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

public class FrenchMatcher extends BaseMatcher {
    private final MarkerDetector markerDetector;

    public FrenchMatcher() {

        String FROM_REGEX_SKELETON = "^De[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^À[\\s]*:.*$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:.*$";
        String DATE_REGEX = "^Envoyé[\\s]*:.*$";
        String SUBJECT_REGEX = "^Objet[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                SUBJECT_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector() {
        return markerDetector;
    }
}
