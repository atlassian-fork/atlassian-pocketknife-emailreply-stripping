package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.substring;

/**
 * Detect quoted email using following pattern:
 * <pre>
 *     Wed, Jun 10, 2015 at 9:59 AM, Joe Doe <jd@gmail.com>:
 *     > quoted text
 * </pre>
 * Supported language: English, French, German, Spanish, Japanese
 */
public class GenericAttributionBlockMatcher extends StatelessQuotedEmailMatcher
{
    /**
     * Limit line lengths in order to avoid regex hangs when parsing wiki style embedded images
     */
    private static final int LINE_LIMIT = 200;

    private final String MARKER_LINE_REGEX = ".*(<|\\[)[^@]*@[^@]*(>|\\])[\\s]*:$";
    private final Pattern MARKER_LINE_PATTERN = Pattern.compile(MARKER_LINE_REGEX, Pattern.CASE_INSENSITIVE);

    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        return doesBeginWithOneLineMarker(textBlock)
                || doesBeginWithTwoLineMarker(textBlock);
    }

    private boolean doesBeginWithOneLineMarker(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 2)
        {
            return false;
        }

        final String firstLine = substring(TextBlockUtil.getLineOrEmptyString(textBlock, 0).trim(), 0, LINE_LIMIT);

        return MARKER_LINE_PATTERN.matcher(firstLine).find()
                && !LineMatchingUtil.isStartWithQuoteLineIndicator(firstLine)
                && isAllQuotedLines(textBlock, 1);
    }

    private boolean doesBeginWithTwoLineMarker(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 3)
        {
            return false;
        }

        final String first2Lines = TextBlockUtil.getFirstTwoLines(textBlock, LINE_LIMIT);
        final String firstLine = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String secondLine = TextBlockUtil.getLineOrEmptyString(textBlock, 1);

        return MARKER_LINE_PATTERN.matcher(first2Lines).find()
                && !LineMatchingUtil.isStartWithQuoteLineIndicator(firstLine)
                && !LineMatchingUtil.isStartWithQuoteLineIndicator(secondLine)
                && isAllQuotedLines(textBlock, 2);
    }

    private boolean isAllQuotedLines(List<String> textBlock, int startIndex)
    {
        if (textBlock == null
                || startIndex < 0
                || startIndex > textBlock.size() - 1)
        {
            return false;
        }

        for (int i = startIndex; i < textBlock.size(); i++)
        {
            final String line = textBlock.get(i);
            if (i == startIndex)
            {
                if (!LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line))
                {
                    return false;
                }
            }
            else if (!LineMatchingUtil.isStartWithQuoteLineIndicator(line))
            {
                return false;
            }
        }
        return true;
    }
}
