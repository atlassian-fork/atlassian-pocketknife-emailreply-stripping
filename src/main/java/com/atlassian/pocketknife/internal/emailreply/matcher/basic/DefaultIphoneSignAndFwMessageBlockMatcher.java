package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using this pattern:
 * <pre>
 *     Sent from my iPhone
 *     (---) Begin forwarded message (---)
 *     > quoted line
 * </pre>
 * Supports English, French, German, Japanese, Spanish
 */
public class DefaultIphoneSignAndFwMessageBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        return RegexList.DEFAULT_IPHONE_SIGNATURE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && LineMatchingUtil.isBlankOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 1), RegexList.BEGIN_FORWARD_MESSAGE_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 2), RegexList.BEGIN_FORWARD_MESSAGE_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 3), RegexList.BEGIN_FORWARD_MESSAGE_PATTERN);
    }
}
