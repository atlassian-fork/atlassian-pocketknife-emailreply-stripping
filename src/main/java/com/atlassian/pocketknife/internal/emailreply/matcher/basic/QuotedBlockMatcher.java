package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email by using these patterns:
 * <br/>
 * Pattern 1
 * <pre>
 *      > On ... someone wrote:
 *      > quoted text
 * </pre>
 * Pattern 2
 * <pre>
 *      | On ... someone wrote:
 *      | quoted text
 * </pre>
 * All lines within provided textBlock must start with either > or |
 * <br/>
 * Supports English, French, German, Spanish
 */
public class QuotedBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        if (textBlock.size() >= 2)
        {
            return isQuotedAttributionLine(TextBlockUtil.getLineOrEmptyString(textBlock, 0))
                    && isQuotedBlock(textBlock);
        }
        else
        {
            return false;
        }
    }

    private boolean isQuotedAttributionLine(final String line)
    {
        return RegexList.QUOTED_ON_DATE_SMB_WROTE_PATTERN.matcher(line).find()
                || RegexList.QUOTED_ON_DATE_WROTE_SMB_PATTERN.matcher(line).find();
    }

    private boolean isQuotedBlock(final List<String> textBlock)
    {
        for (final String line: textBlock)
        {
            if (!LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line))
            {
                return false;
            }
        }
        return true;
    }
}
