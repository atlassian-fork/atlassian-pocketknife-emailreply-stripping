package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

public class SpanishMatcher extends BaseMatcher {
    private final MarkerDetector markerDetector;

    public SpanishMatcher() {

        String FROM_REGEX_SKELETON = "^De[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^Para[\\s]*:.*$";
        String CC_REGEX_SKELETON = "^CC[\\s]*:.*$";
        String DATE_REGEX = "^Enviado el[\\s]*:.*$";
        String SUBJECT_REGEX = "^Asunto[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                SUBJECT_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector() {
        return markerDetector;
    }
}
