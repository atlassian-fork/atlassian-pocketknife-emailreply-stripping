package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.v9;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample quoted text beginning marker: <br/>
 * To:		chuongnn.atlassian.cus1@gmail.com <br/>
 * <br/>
 * cc:	<br/>
 * <br/>
 * Subject:		Re: IBM Notes 9 hello English empire 1  <br/>
 * </p>
 * <p>
 * <p> Matching logic: <br/>
 * 1. Text block has at least 5 lines. <br/>
 * 2. First five lines perfectly match pattern of "To..cc..Subject..". <br/>
 * </p>
 */
public class Matcher implements QuotedEmailMatcher
{

    private final String EMAIL_REGEX = "[^@]+@[^@]";

    private final String REGEX_SKELETON = "^To:[\\s]+${0}+\\n\\ncc.*\\n\\nSubject:.*\\n";

    /**
     * package private for testing.
     */
    final Pattern PATTERN = Pattern.compile(makeRegex(REGEX_SKELETON), Pattern.CASE_INSENSITIVE);

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() < 5)
        {
            return false;
        }
        else
        {
            StringBuilder textBuilder = new StringBuilder();
            for (int i = 0; i < textBlock.size(); i++)
            {
                textBuilder.append(textBlock.get(i).trim() + "\n");
            }

            return RegexUtils.match(PATTERN, textBuilder.toString());
        }
    }

    String makeRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, EMAIL_REGEX);
    }
}
