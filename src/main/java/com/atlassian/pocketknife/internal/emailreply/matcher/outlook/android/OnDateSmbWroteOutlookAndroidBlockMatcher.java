package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.android;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using following pattern:
 * <pre>
 *
 *
 *
 *     On Wed, Jun 10, 2015 at 9:59 AM, Joe Doe <jd@gmail.com> wrote:
 * </pre>
 * Supported languages: English, French, Spanish
 */
public class OnDateSmbWroteOutlookAndroidBlockMatcher extends StatelessQuotedEmailMatcher {

    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock) {
        if (textBlock.isEmpty()) {
            return false;
        }

        final String firstLine = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String secondLine = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String thirdLine = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
        final String last2Lines = TextBlockUtil.get2LinesOrEmptyString(textBlock, 3);

        return StringUtils.isBlank(firstLine)
                && StringUtils.isBlank(secondLine)
                && StringUtils.isBlank(thirdLine)
                && RegexList.ON_DATE_SMB_WROTE_PATTERN.matcher(last2Lines).find();
    }

}
