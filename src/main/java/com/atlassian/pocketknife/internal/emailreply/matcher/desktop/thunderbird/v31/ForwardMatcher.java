package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample forward quoted text beginning marker: <br/>
 * -------- Forwarded Message -------- <br/>
 * blah blah <br/>
 * blah blah <br/>
 * <p>
 * <p> Matching logic: <br/>
 * 1. Text block begins with a attribution line. <br/>
 * </p>
 */
public class ForwardMatcher implements QuotedEmailMatcher
{

    private final String ATTRIBUTION_LINE_REGEX_SKELETON = "^[-]+[\\s]+${0}[\\s]+${1}[\\s]+[-]+$";

    /**
     * package private for testing.
     */
    final Pattern ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN = Pattern.compile(
            makeRegex("Forwarded", "Message"), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_PATTERN = Pattern.compile(
            makeRegex("Weitergeleitete", "Nachricht"), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_PATTERN = Pattern.compile(
            makeRegex("Mensaje", "reenviado"), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_PATTERN = Pattern.compile(
            makeRegex("Message", "transfÃ©rÃ©"), Pattern.CASE_INSENSITIVE);
    final List<Pattern> ATTRIBUTION_LINE_PATTERNS = Lists.newArrayList(
            ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN,
            ATTRIBUTION_LINE_FRENCH_PATTERN,
            ATTRIBUTION_LINE_GERMAN_PATTERN,
            ATTRIBUTION_LINE_SPANISH_PATTERN);

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() == 0)
        {
            return false;
        }
        else
        {
            return isAttributionLine(textBlock.get(0));
        }
    }

    boolean isAttributionLine(String line)
    {
        for (Pattern pattern : ATTRIBUTION_LINE_PATTERNS)
        {
            if (RegexUtils.match(pattern, line))
            {
                return true;
            }
        }
        return false;
    }

    String makeRegex(String... paramValues)
    {
        return RegexUtils.buildRegexFromSkeleton(ATTRIBUTION_LINE_REGEX_SKELETON, paramValues);
    }
}
