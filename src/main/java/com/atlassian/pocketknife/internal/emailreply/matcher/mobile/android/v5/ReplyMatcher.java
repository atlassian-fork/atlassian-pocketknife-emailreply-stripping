package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Pattern;

public class ReplyMatcher implements QuotedEmailMatcher
{
    private final String TIME_REGEX = "[0-9]+:[0-9]+[\\s]+(PM|AM)";
    private final String MILITARY_TIME_REGEX = "[0-9]+:[0-9]+";
    private final String NAME_REGEX = "[^<]+<[^@]+@[^>]+>";

    private final String ATTRIBUTION_LINE_ENGLISH_REGEX_SKELETON = "^[\\s]*On.*[\\s]+${0},[\\s]*${1}[\\s]*wrote:$";
    private final String ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON = "^[\\s]*Le.*[\\s]+${0},[\\s]+${1}[\\s]+a[\\s]*écrit[\\s]*:$";
    private final String ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON = "^[\\s]*Am.*${0}.*schrieb${1}:$";
    private final String ATTRIBUTION_LINE_JAPANESE_REGEX_SKELETON = "^[\\s]*[0-9]+\\/[0-9]+\\/[0-9]+.*${0}[\\s]*${1}[\\s]*:$";
    private final String ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON = "^[\\s]*El.*[\\s]+${0}.*${1}[\\s]*escribió:$";
    private final String QUOTED_LINE_REGEX = "^>.*$";

    /**
     * package private for testing.
     */
    final Pattern ATTRIBUTION_LINE_ENGLISH_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_ENGLISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_JAPANESE_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_JAPANESE_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_ENGLISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_JAPANESE_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern QUOTED_LINE_PATTERN = Pattern.compile(QUOTED_LINE_REGEX, Pattern.CASE_INSENSITIVE);

    final List<Pattern> ATTRIBUTION_LINE_PATTERNS = Lists.newArrayList(
            ATTRIBUTION_LINE_ENGLISH_PATTERN,
            ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_PATTERN,
            ATTRIBUTION_LINE_FRENCH_PATTERN,
            ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_PATTERN,
            ATTRIBUTION_LINE_GERMAN_PATTERN,
            ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_PATTERN,
            ATTRIBUTION_LINE_JAPANESE_PATTERN,
            ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_PATTERN,
            ATTRIBUTION_LINE_SPANISH_PATTERN,
            ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_PATTERN);

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() < 3)
        {
            return false;
        }

        //Match attribution line
        boolean attributionLineMatched = false;
        StringBuilder textBuilder = new StringBuilder();
        int readLineIndex = -1;

        for (int i = 0; i < textBlock.size(); i++)
        {
            textBuilder.append(textBlock.get(i));
            readLineIndex = i;
            attributionLineMatched |= isAttributionLine(textBuilder.toString());
            if (attributionLineMatched)
            {
                break;
            }
        }
        if (!attributionLineMatched ||
                readLineIndex >= textBlock.size() - 2)
        {
            return false;
        }

        //Match blank line between attribution line and first quoted line
        readLineIndex++;
        if (!textBlock.get(readLineIndex).trim().isEmpty())
        {
            return false;
        }
        readLineIndex++;

        //Match quoted line
        boolean allQuotedLinesMatched = true;
        for (int i = readLineIndex; i < textBlock.size(); i++)
        {
            allQuotedLinesMatched &= isQuotedLine(textBlock.get(i));
            if (!allQuotedLinesMatched)
            {
                break;
            }
        }

        return allQuotedLinesMatched;
    }

    boolean isAttributionLine(String line)
    {
        for (Pattern pattern : ATTRIBUTION_LINE_PATTERNS)
        {
            if (RegexUtils.match(pattern, line))
            {
                return true;
            }
        }
        return false;
    }

    boolean isQuotedLine(String line)
    {
        return RegexUtils.match(QUOTED_LINE_PATTERN, line);
    }

    String makeReplyRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, TIME_REGEX, NAME_REGEX);
    }

    String makeMilitaryTimeReplyRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, MILITARY_TIME_REGEX, NAME_REGEX);
    }
}
