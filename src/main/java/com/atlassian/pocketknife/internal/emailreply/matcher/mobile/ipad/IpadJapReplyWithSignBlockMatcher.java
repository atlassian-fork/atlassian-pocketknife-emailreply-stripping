package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect the following pattern:
 * <br/>
 * <pre>
 *      iPadから送信 (sent from my ipad)
 *      date, someone のメッセージ (message of someone on date)
 *      > quoted text
 * </pre>
 * Support Japanese only!!
 */
public class IpadJapReplyWithSignBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 3)
        {
            return false;
        }

        return RegexList.DEFAULT_IPAD_SIGNATURE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && LineMatchingUtil.isBlankOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 1), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 2), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 3), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN);
    }
}
