package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Supported patterns:
 * <pre>
 *     Sent from my iPad
 *     (optional blank line)
 *     Begin forwarded message
 *     (optional blank line)
 * </pre>
 * Supported languages: English, French, German, Spanish
 */
public class IpadForwardWithSignBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 2)
        {
            return false;
        }

        return RegexList.DEFAULT_IPAD_SIGNATURE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && isBlockHasBeginForwardedMessageLine(textBlock);
    }

    private boolean isBlockHasBeginForwardedMessageLine(final List<String> textBlock)
    {
        for (final String line: textBlock)
        {
            if (RegexList.BEGIN_FORWARD_MESSAGE_PATTERN.matcher(line).find())
            {
                return true;
            }
        }
        return false;
    }
}
