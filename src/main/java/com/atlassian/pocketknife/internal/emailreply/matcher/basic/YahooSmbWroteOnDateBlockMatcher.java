package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Special implementation for Yahoo:
 * <pre>
 *     {5 empty spaces}Joe Doe <jd@gmail.com> wrote on Wed, Jun 10, 2015 at 9:59 AM:
 *     empty line
 *     empty line
 *     {1 empty space} quoted text
 * </pre>
 * Supported language: German
 */
public class YahooSmbWroteOnDateBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        return RegexList.YAHOO_SMB_WROTE_ON_DATE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && StringUtils.isBlank(TextBlockUtil.getLineOrEmptyString(textBlock, 1))
                && StringUtils.isBlank(TextBlockUtil.getLineOrEmptyString(textBlock, 2))
                && TextBlockUtil.getLineOrEmptyString(textBlock, 3).startsWith(" ");
    }
}
