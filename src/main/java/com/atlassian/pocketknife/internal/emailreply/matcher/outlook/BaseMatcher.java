package com.atlassian.pocketknife.internal.emailreply.matcher.outlook;

import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public abstract class BaseMatcher implements QuotedEmailMatcher
{
    protected interface QuotedTextMarkerDetector
    {
        List<Pattern> getFirstLinePatterns();

        List<Pattern> getSecondLinePatterns();
    }

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        QuotedTextMarkerDetector markerDetector = getMarkerDetector();
        if (markerDetector == null)
        {
            return false;
        }

        List<Pattern> firstLinePatterns = markerDetector.getFirstLinePatterns();
        List<Pattern> secondLinePatterns = markerDetector.getSecondLinePatterns();
        if (firstLinePatterns == null ||
                firstLinePatterns.isEmpty() ||
                secondLinePatterns == null ||
                secondLinePatterns.isEmpty())
        {
            return false;
        }

        return isQuotedText(
                textBlock, firstLinePatterns, secondLinePatterns);
    }

    protected abstract QuotedTextMarkerDetector getMarkerDetector();

    protected boolean isQuotedText(List<String> textBlock,
            Collection<Pattern> firstLinePatterns,
            Collection<Pattern> secondLinePatterns)
    {
        if (textBlock == null || textBlock.size() < 2)
        {
            return false;
        }

        // Match first line
        StringBuilder lineBuilder = new StringBuilder();
        int readLineIndex = -1;
        Boolean isFirstLineMatched = false;
        for (int i = 0; i < textBlock.size(); i++)
        {
            readLineIndex = i;
            lineBuilder.append(textBlock.get(i));
            isFirstLineMatched = isQuotedLine(
                    lineBuilder.toString(), firstLinePatterns);
            if (isFirstLineMatched)
            {
                break;
            }
        }
        if (!isFirstLineMatched || readLineIndex == textBlock.size() - 1)
        {
            return false;
        }

        // Match second line
        lineBuilder.setLength(0);
        boolean isSecondLineMatched = false;
        for (int i = readLineIndex + 1; i < textBlock.size(); i++)
        {
            lineBuilder.append(textBlock.get(i));
            isSecondLineMatched = isQuotedLine(
                    lineBuilder.toString(), secondLinePatterns);
            if (isSecondLineMatched)
            {
                break;
            }
        }
        return isSecondLineMatched;
    }

    protected boolean isQuotedLine(String line, Collection<Pattern> patterns)
    {
        for (Pattern pattern : patterns)
        {
            if (RegexUtils.match(
                    pattern, line))
            {
                return true;
            }
        }
        return false;
    }
}
