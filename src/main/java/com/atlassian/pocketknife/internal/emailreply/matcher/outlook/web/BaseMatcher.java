package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample quoted text beginning marker: <br/>
 * ________________________________
 * From: Matthew McMahonTest
 * Sent: Friday, 13 January 2017 12:27:21 PM
 * To: mmcmahon@atlassian.com
 * Subject: Test
 * <p>
 * <p> Matching logic: <br/>
 * 1. Text block has at least 2 lines. <br/>
 * </p>
 */
public abstract class BaseMatcher
        extends com.atlassian.pocketknife.internal.emailreply.matcher.outlook.BaseMatcher {

    public static final String OUTLOOK_WEB_HORIZONTAL_RULE = "________________________________";

    protected class MarkerDetector implements QuotedTextMarkerDetector {
        private final String NAME_WITH_EMAIL_REGEX = "[\\s]*[^\\<]+\\<[^@]+@[^\\>]+\\>\\>?[\\s]*";

        /**
         * package private for testing.
         */
        final Pattern FROM_PATTERN;
        final Pattern TO_PATTERN;
        final Pattern CC_PATTERN;
        final Pattern DATE_PATTERN;
        final Pattern SUBJECT_PATTERN;
        final Pattern HORIZONTAL_RULE_PATTERN;
        final List<Pattern> FIRST_LINE_PATTERNS;
        final List<Pattern> SECOND_LINE_PATTERNS;

        public MarkerDetector(String FROM_REGEX_SKELETON,
                              String TO_REGEX_SKELETON,
                              String CC_REGEX_SKELETON,
                              String DATE_REGEX,
                              String SUBJECT_REGEX) {
            FROM_PATTERN = Pattern.compile(
                    makeRegex(FROM_REGEX_SKELETON, NAME_WITH_EMAIL_REGEX), Pattern.CASE_INSENSITIVE);
            TO_PATTERN = Pattern.compile(TO_REGEX_SKELETON, Pattern.CASE_INSENSITIVE);
            CC_PATTERN = Pattern.compile(CC_REGEX_SKELETON, Pattern.CASE_INSENSITIVE);
            DATE_PATTERN = Pattern.compile(DATE_REGEX, Pattern.CASE_INSENSITIVE);
            SUBJECT_PATTERN = Pattern.compile(SUBJECT_REGEX, Pattern.CASE_INSENSITIVE);
            HORIZONTAL_RULE_PATTERN = Pattern.compile("^" + OUTLOOK_WEB_HORIZONTAL_RULE + "$", Pattern.CASE_INSENSITIVE);

            FIRST_LINE_PATTERNS = Collections.unmodifiableList(
                    Lists.newArrayList(
                            HORIZONTAL_RULE_PATTERN, FROM_PATTERN, TO_PATTERN, CC_PATTERN, DATE_PATTERN, SUBJECT_PATTERN));
            SECOND_LINE_PATTERNS = Collections.unmodifiableList(
                    Lists.newArrayList(
                            FROM_PATTERN, TO_PATTERN, CC_PATTERN, DATE_PATTERN, SUBJECT_PATTERN));
        }

        @Override
        public List<Pattern> getFirstLinePatterns() {
            return FIRST_LINE_PATTERNS;
        }

        @Override
        public List<Pattern> getSecondLinePatterns() {
            return SECOND_LINE_PATTERNS;
        }

        private String makeRegex(String skeleton, String nameRegex) {
            return RegexUtils.buildRegexFromSkeleton(
                    skeleton, nameRegex);
        }
    }

    protected abstract MarkerDetector getMarkerDetector();
}