package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

public class FrenchMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public FrenchMatcher()
    {
        String FROM_REGEX_SKELETON = "^De[\\s]*:${0}$";
        String REPLY_TO_REGEX_SKELETON = "^Répondre[\\s]+à[\\s]*:${0}$";
        String TO_REGEX_SKELETON = "^À[\\s]*:${0}$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:${0}$";
        String DATE_REGEX = "^Date[\\s]*:.*$";
        String DATE_AT_TOP_REGEX = "^Date[\\s]*:.*(De|Répondre à|À|Cc|Objet)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Objet[\\s]*:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Objet[\\s]*:.*(De|Répondre à|À|Cc|Date)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                REPLY_TO_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
