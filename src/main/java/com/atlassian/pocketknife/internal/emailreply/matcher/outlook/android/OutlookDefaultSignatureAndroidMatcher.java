package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.android;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using this pattern:
 * <pre>
 *     Get Outlook for Android<https://aka.ms/ghei36>
 *     ________________________________
 * </pre>
 * Supports English, French, German, Spanish
 */
public class OutlookDefaultSignatureAndroidMatcher extends StatelessQuotedEmailMatcher {

    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock) {
        if (textBlock.isEmpty()) {
            return false;
        }

        final String firstLine = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String secondLine = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String thirdLine = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
        final String fourthLine = TextBlockUtil.getLineOrEmptyString(textBlock, 3);

        return RegexList.DEFAULT_OUTLOOK_ANDROID_SIGNATURE_PATTERN.matcher(firstLine).find()
                && StringUtils.isBlank(secondLine)
                && StringUtils.isBlank(thirdLine)
                && StringUtils.isBlank(fourthLine);
    }

}
