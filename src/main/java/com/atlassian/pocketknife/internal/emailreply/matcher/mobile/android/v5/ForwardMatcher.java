package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;

import java.util.List;

public class ForwardMatcher implements QuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() == 0)
        {
            return false;
        }

        return isAttributionLine(textBlock.get(0));
    }

    boolean isAttributionLine(String line)
    {
        return RegexUtils.match(RegexList.FORWARDED_MESSAGE_PATTERN, line);
    }
}
