package com.atlassian.pocketknife.internal.emailreply.matcher;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DelegatingQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.ForwardedMessageBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.OriginalMessageBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.YahooOnDateSmbWroteBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.YahooSmbWroteOnDateBlockMatcher;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Detector for emails sent by Yahoo web client
 */
public class YahooReplyMatcher extends DelegatingQuotedEmailMatcher
{
    @Override
    protected List<QuotedEmailMatcher> createDelegators()
    {
        return Lists.<QuotedEmailMatcher>newArrayList(
                new YahooOnDateSmbWroteBlockMatcher(),
                new YahooSmbWroteOnDateBlockMatcher(),
                new OriginalMessageBlockMatcher(),
                new ForwardedMessageBlockMatcher()
        );
    }
}
