package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

public class SpanishMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public SpanishMatcher()
    {
        String FROM_REGEX_SKELETON = "^De[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^Para[\\s]*:${0}$";
        String CC_REGEX_SKELETON = "^CC[\\s]*:${0}$";
        String DATE_REGEX = "^Enviado el[\\s]*:.*$";
        String DATE_AT_TOP_REGEX = "^Enviado el[\\s]*:.*(De|Para|Cc|Asunto)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Asunto:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Asunto:.*(De|Para|Cc|Enviado el)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
