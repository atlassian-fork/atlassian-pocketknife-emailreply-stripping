package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.google.common.collect.Lists;

import java.util.List;

public class Matcher extends ProxyQuotedEmailMatcher
{
    private final List<QuotedEmailMatcher> delegators = Lists.<QuotedEmailMatcher>newArrayList(
            new EnglishMatcher(),
            new FrenchMatcher(),
            new GermanMatcher(),
            new JapaneseMatcher(),
            new SpanishMatcher());

    @Override
    protected List<QuotedEmailMatcher> getDelegators()
    {
        return delegators;
    }
}
