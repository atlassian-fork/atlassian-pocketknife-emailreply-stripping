package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

public class EnglishMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public EnglishMatcher()
    {
        String FROM_REGEX_SKELETON = "^From[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^To[\\s]*:${0}$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:${0}$";
        String DATE_REGEX = "^Sent[\\s]*:.*$";
        String DATE_AT_TOP_REGEX = "^Sent[\\s]*:.*(From|To|Cc|Subject)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Subject[\\s]*:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Subject[\\s]*:.*(From|To|Cc|Sent)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
