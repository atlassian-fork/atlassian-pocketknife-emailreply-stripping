package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect the following pattern:
 * <br/>
 * <pre>
 *      date, someone のメッセージ (message of someone on date)
 *      > quoted text or blank line
 *      > quoted text or blank line
 *      > quoted text
 * </pre>
 * Supports Japanese only!!
 */
public class IpadJapReplyWithoutSignBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 2)
        {
            return false;
        }
        else if (textBlock.size() == 2)
        {
            return isAttributionLine(TextBlockUtil.getLineOrEmptyString(textBlock, 0))
                    && LineMatchingUtil.isStartWithQuoteLineIndicator(TextBlockUtil.getLineOrEmptyString(textBlock, 1));
        }
        else
        {
            final String firstLine = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
            final List<String> middleLines = textBlock.subList(1, textBlock.size() - 2);
            final String lastLine = TextBlockUtil.getLineOrEmptyString(textBlock, textBlock.size() - 1);

            return isAttributionLine(firstLine)
                    && isBlockQuotedOrBlank(middleLines)
                    && LineMatchingUtil.isStartWithQuoteLineIndicator(lastLine);
        }
    }

    private boolean isAttributionLine(final String line)
    {
        return RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN.matcher(line).find();
    }

    private boolean isBlockQuotedOrBlank(final List<String> block)
    {
        for (final String line : block)
        {
            if (!LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line))
            {
                return false;
            }
        }
        return true;
    }
}
