package com.atlassian.pocketknife.internal.emailreply.matcher.util;

import com.google.common.collect.Maps;
import org.apache.commons.lang.text.StrSubstitutor;

import java.util.Map;
import java.util.regex.Pattern;

public class RegexUtils
{

    /**
     * Skeleton must have form of "...${i}..." where i must have integral value.
     * Given skeleton is "---${0} ${1}---" and paramValues are {0 -> "Forwarded"; 1 -> "Message"},
     * the result regex is "---Forwarded Message---".
     *
     * @param skeleton    skeleton of regex
     * @param paramValues substituted values
     * @return resulted regex
     */
    public static String buildRegexFromSkeleton(String skeleton, String... paramValues)
    {
        Map<String, String> params = Maps.newHashMap();
        for (int i = 0; i < paramValues.length; i++)
        {
            params.put(String.valueOf(i), paramValues[i]);
        }
        return StrSubstitutor.replace(skeleton, params, "${", "}");
    }

    public static boolean match(Pattern pattern, String text)
    {
        return pattern.matcher(text).find();
    }
}
