package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

public class EnglishMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public EnglishMatcher()
    {
        String FROM_REGEX_SKELETON = "^From:${0}$";
        String REPLY_TO_REGEX_SKELETON = "^Reply-To:${0}$";
        String TO_REGEX_SKELETON = "^To:${0}$";
        String CC_REGEX_SKELETON = "^Cc:${0}$";
        String DATE_REGEX = "^Date:.*$";
        String DATE_AT_TOP_REGEX = "^Date:.*(From|Reply-To|To|Cc|Subject)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Subject:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Subject:.*(From|Reply-To|To|Cc|Date)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                REPLY_TO_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
