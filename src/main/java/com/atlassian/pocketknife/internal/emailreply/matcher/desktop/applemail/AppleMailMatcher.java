package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.v8.Matcher;
import com.google.common.collect.Lists;

import java.util.List;

public class AppleMailMatcher extends ProxyQuotedEmailMatcher
{
    private final List<QuotedEmailMatcher> delegators = Lists.<QuotedEmailMatcher>newArrayList(new Matcher());

    @Override
    protected List<QuotedEmailMatcher> getDelegators()
    {
        return delegators;
    }
}

