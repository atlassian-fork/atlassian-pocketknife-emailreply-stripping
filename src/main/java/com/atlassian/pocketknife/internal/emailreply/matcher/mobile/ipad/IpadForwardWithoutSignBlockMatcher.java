package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Supported pattern:
 * <pre>
 *     Begin forwarded message
 *     > quoted text or blank line
 *     > quoted text or blank line
 *     > quoted text
 * </pre>
 * Supported languages: English, French, German, Spanish
 */
public class IpadForwardWithoutSignBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() >= 4)
        {
            final String line1 = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
            final String line2 = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
            final String line3 = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
            final String line4 = TextBlockUtil.getLineOrEmptyString(textBlock, 3);

            return RegexList.BEGIN_FORWARD_MESSAGE_PATTERN.matcher(line1).find()
                    && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line2)
                    && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line3)
                    && LineMatchingUtil.isStartWithQuoteLineIndicator(line4);
        }
        else
        {
            return false;
        }
    }
}
