package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

public class EnglishMatcher extends BaseMatcher {
    private final MarkerDetector markerDetector;

    public EnglishMatcher() {
        String FROM_REGEX_SKELETON = "^From[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^To[\\s]*:.*$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:.*$";
        String DATE_REGEX = "^Sent[\\s]*:.*$";
        String SUBJECT_REGEX = "^Subject[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                SUBJECT_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector() {
        return markerDetector;
    }
}
