package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.MailUtils;
import com.atlassian.mail.options.GetBodyOptions;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.util.EmailTextReader;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.stripToEmpty;

/**
 * This class incrementally read the text body in block of 5 lines and uses
 * a list of {@link QuotedEmailMatcher}
 * to detect if the block is the start of a quoted email. If the block is the start of a quoted
 * email, then everything before that block is returned. This class never returns null
 */
@ParametersAreNonnullByDefault
public class DefaultEmailReplyCleaner implements EmailReplyCleaner {
    private static final Logger log = LoggerFactory.getLogger(DefaultEmailReplyCleaner.class);
    private final List<QuotedEmailMatcher> matchers;

    public DefaultEmailReplyCleaner(List<QuotedEmailMatcher> matchers) {
        // linked hash set to maintain ordering, but remove duplicates
        this.matchers = ImmutableList.copyOf(newLinkedHashSet(checkNotNull(matchers, "matchers can not be null")));
    }

    @Nonnull
    @Override
    public EmailCleanerResult cleanQuotedEmail(@Nonnull Message message) throws MessagingException, IOException {
        final String body;
        try {
            // don't strip in MailUtils, but do it ourselves instead
            body = MailUtils.getBody(message, GetBodyOptions.PREFER_TEXT_BODY_NO_STRIP_WHITESPACE);
        } catch (MessagingException e) {
            log.warn("Failed to retrieve text body from message", e);
            throw e;
        }

        return cleanQuotedEmailInternal(body);
    }

    /**
     * package scoped for use by {@link HtmlEmailReplyCleaner}
     */
    EmailCleanerResult cleanQuotedEmailInternal(@Nullable final String body) {
        if (isBlank(body)) {
            return new EmailCleanerResult(EMPTY, defaultIfEmpty(body, EMPTY));
        }

        final String updatedBody = body
                .replaceAll("\\r\\n", "\n")
                .replaceAll("\\r", "\n");

        final EmailTextReader reader = new EmailTextReader(updatedBody);

        List<String> block;
        final StringBuilder strippedBodyBuilder = new StringBuilder();
        do {
            block = reader.readNextBlock();
            if (block.isEmpty()) {
                break;
            } else if (!isQuotedBlock(block)) {
                strippedBodyBuilder.append(block.get(0));
                strippedBodyBuilder.append("\n");
            } else {
                break;
            }
        } while (!block.isEmpty());

        return new EmailCleanerResult(stripToEmpty(strippedBodyBuilder.toString()), updatedBody);
    }

    boolean isQuotedBlock(List<String> block) {
        for (QuotedEmailMatcher matcher : matchers) {
            if (matcher.isQuotedEmail(block)) {
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    List<QuotedEmailMatcher> getMatchers() {
        return matchers;
    }
}
