package com.atlassian.pocketknife.api.emailreply;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Implementation of this interface can be used to detect if the body text of the
 * provided message contains a quoted email. If the body contains a quoted email,
 * then that quoted email is removed from the body. Attachments are not affected
 */
@ParametersAreNonnullByDefault
public interface EmailReplyCleaner {
    /**
     * Analyze the body text to detect quoted emails and remove them.
     * Implementation of this method must not returns null
     *
     * @param message the message to be analyzed
     * @return the cleaner result, never null
     */
    @Nonnull
    EmailCleanerResult cleanQuotedEmail(Message message) throws MessagingException, IOException;

    /**
     * The result of processing the supplied {@link Message}
     */
    @ParametersAreNonnullByDefault
    class EmailCleanerResult {

        private final String originalBody;
        private final String body;

        public EmailCleanerResult(String body, String originalBody) {
            this.body = checkNotNull(body, "body can not be null");
            this.originalBody = checkNotNull(originalBody, "original body can not be null");
        }

        /**
         * body of the text message without quoted email, never returns null but may be identical
         * to {@link #getOriginalBody()} if no cleaning occured.
         * <p>
         * If the body is blank, then original body will be returned from this method, as there is a
         * possibility that the whole email was stripped, which is undesireable.
         * <p>
         * If you want the body, whether empty or not, then use {@link #getRawBody()}
         *
         * @return The body with cleaning performed
         */
        public String getBody() {
            if (isBlank(body)) {
                return originalBody;
            }
            return body;
        }

        /**
         * Return the raw stripped body, whether blank or not, but never null.
         * <p>
         * Generally it is preferred to use {@link #getBody()}
         *
         * @return The actual stripped body
         */
        public String getRawBody() {
            return body;
        }

        /**
         * The original body retrieved from {@link Message} without any cleaning performed
         *
         * @return the original body, never returns null
         */
        public String getOriginalBody() {
            return originalBody;
        }
    }
}
